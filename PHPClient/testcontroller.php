<?php
//
//version: 0.16.0
//

include('phpconnector.php');
session_start(); // Starting Session
$error=''; // Variable To Store Error Message
$errorget='';
$errorput='';
$errorput2='';
$errorput3='';
$errorput4='';
$errorput5='';
$errorput6='';
$errordel='';
$erroruse='';
$token='';
$text0='';

if (isset($_POST['submit'])) {
	
}

if (isset($_POST['submitBusiness0'])) {
	$text0 = '' ;
	
	//get token from keycloak
	$token=get_token();

	$text0=getCapabilityTypes($token);
	
}

if (isset($_POST['submitBusiness1'])) {
	$text0 = '' ;
		
	//get token from keycloak
	$token=get_token();

	$text0=getBusinessProcesses($token);
	
}

if (isset($_POST['submitBusiness2'])) {
	$text0 = '' ;

	$value = $_POST['idCapabilityType'];
	
	if (empty($value)){
		$errorget = "Id Capability Type is empty";
	}else{
	//get token from keycloak
	$token=get_token();

	$text0=getCapabilityType($token, $value);
	}
	
}

if (isset($_POST['submitBusiness3'])) {
	$text0 = '' ;

	$value = $_POST['idBusiness'];
	
	if (empty($value)){
		$errorget = "Id Business is empty";
	}else{
		//get token from keycloak
		$token=get_token();

		$text0=getBusinessProcess($token, $value);
	}
	
}

if (isset($_POST['submitBusiness4'])) {
	$text0 = '' ;

	//get token from keycloak
	$token=get_token();

	$text0=getCapabilities($token);
	
}

if (isset($_POST['submitBusiness5'])) {
	$text0 = '' ;

	$value = $_POST['idCapability'];
	
	if (empty($value)){
		$errorget = "Id Capability is empty";
	}else{
		//get token from keycloak
		$token=get_token();

		$text0=getCapability($token, $value);
	}
	
}

if (isset($_POST['submitBusiness6'])) {
	$text0 = '' ;
	
	//get token from keycloak
	$token=get_token();

	$text0=getBusinessProcessTemplates($token);
	
}

if (isset($_POST['submitBusiness15'])) {
	$text0 = '' ;

	$value = $_POST['idBusinessTemplate'];

	if (empty($value)){
		$errorget = "Id Business Template is empty";
	}else{
		//get token from keycloak
		$token=get_token();

		$text0=getBusinessProcessTemplate($token, $value);
	}

}

if (isset($_POST['submitBusiness7'])) {
	$body = '' ;
	
	$desc = $_POST['descCapability'];
	$uri = $_POST['uriCapability'];
	$cte = $_POST['cteCapability'];
	
	if (empty($desc) || empty($uri) || empty($cte)){
		$errorput="Description, URI or Capability is invalid";
	}else{
		
		//get token from keycloak
		$token=get_token();
		
		//call to REST
		//using a standard class to save data
		$capability = new stdClass;
		$capability->Capability->description = $desc;
		$capability->Capability->uri = $uri;
		$capability->Capability->cte_id = $cte;
		$errorput=updateCapability($token, $capability);
	}		
	
}

if (isset($_POST['submitBusiness8'])) {
	$body = '' ;

	$name = $_POST['nameCapabilityType'];
	$reqmsgtype = $_POST['reqmsgtypeCapabilityType'];
	$resmsgtype = $_POST['resmsgtypeCapabilityType'];
	$schema = $_POST['schemaCapabilityType'];
	$context = $_POST['contextCapabilityType'];
	
	if (empty($name) || empty($reqmsgtype) || empty($resmsgtype
			|| empty($schema) || empty($context))){
		$errorput2="Name, Request, Response, Schema or Context is invalid";
	}else{
			
		//get token from keycloak
		$token=get_token();
			
		//call to REST
		//using a standard class to save data
		$capabilityType = new stdClass;
		$capabilityType->capabilityType->name = $name;
		$capabilityType->capabilityType->requestMessageType = $reqmsgtype;
		$capabilityType->capabilityType->responseMessageType = $resmsgtype;
		$capabilityType->capabilityType->schemaLocation = $schema;
		$capabilityType->capabilityType->contextPath = $context;
		$errorput2=updateCapabilityType($token, $capabilityType);
	}

	
}

if (isset($_POST['submitBusiness9'])) {
	$body = '' ;

	$name = $_POST['nameBPT'];
	$capabilitiesBPT = $_POST['capabilitiesBPT'];
	$listCapabilityTypes = explode(" ", $capabilitiesBPT);
	
	if (empty($name) || empty($capabilitiesBPT)){
		$errorput3="Name or Capability Type IDs is invalid";
	}else{

		//get token from keycloak
		$token=get_token();

		//call to REST
		//using a standard class to save data
		$businessProcessTemplate = new stdClass;
		$businessProcessTemplate->businessProcessTemplate ->name = $name;
		$businessProcessTemplate->businessProcessTemplate-> listCapabilityTypes = $listCapabilityTypes;
		$errorput3=updateBusinessProcessTemplate($token, $businessProcessTemplate);
	}

	
}


if (isset($_POST['submitBusiness17'])) {
	$body = '' ;

	$name = $_POST['nameBPTR'];
	$idrole1 = $_POST['idrole1BPT'];
	$capabilitiesBPTR1 = $_POST['capabilitiesBPTR1'];
	$listCapabilityTypesR1 = explode(" ", $capabilitiesBPTR1);
	$idrole2 = $_POST['idrole2BPT'];
	$capabilitiesBPTR2 = $_POST['capabilitiesBPTR2'];
	$listCapabilityTypesR2 = explode(" ", $capabilitiesBPTR2);

	if (empty($name)){
		$errorput5="Name is invalid";
	}else{

		//get token from keycloak
		$token=get_token();

		//call to REST
		//using a standard class to save data
		$listRoles = array();
		
		if (!empty($idrole1) && !empty($capabilitiesBPTR1)){
			$role = new stdClass;
			$role->id = $idrole1;
			$role->listCapabilityTypes=$listCapabilityTypesR1;
			array_push($listRoles, $role);
		}
				
		if (!empty($idrole2) && !empty($capabilitiesBPTR2)){
			$role = new stdClass;
			$role->id = $idrole2;
			$role->listCapabilityTypes=$listCapabilityTypesR2;
			array_push($listRoles, $role);
		}
		 
		$businessProcessTemplate = new stdClass;
		$businessProcessTemplate->businessProcessTemplate ->name = $name;
		$businessProcessTemplate->businessProcessTemplate ->listRoles = $listRoles;
		$errorput5=updateBusinessProcessTemplateRole($token, $businessProcessTemplate);
	}


}


if (isset($_POST['submitBusiness10'])) {
	$body = '' ;

	$name = $_POST['nameBP'];
	$bptId = $_POST['bptId'];
	$capabilitiesBP = $_POST['capabilitiesBP'];
	$listCapabilities = explode(" ", $capabilitiesBP);

	if (empty($name) || empty($bptId) || empty($capabilitiesBP)){
		$errorput4="Name, Bpt_id or Capability IDs is invalid";
	}else{

		//get token from keycloak
		$token=get_token();

		//call to REST
		//using a standard class to save data
		$businessProcess = new stdClass;
		$businessProcess->businessProcess ->name = $name;
		$businessProcess->businessProcess ->bptId = $bptId;
		$businessProcess->businessProcess-> listCapabilities = $listCapabilities;
					
		$errorput4=updateBusinessProcess($token, $businessProcess);
	}
	
}

if (isset($_POST['submitBusiness18'])) {
	$body = '' ;

	$name = $_POST['nameBPR'];
	$bptId = $_POST['bptIdR'];
	$idrole1 = $_POST['idrole1BP'];
	$capabilitiesBPR1 = $_POST['capabilitiesBPR1'];
	$listCapabilitiesR1 = explode(" ", $capabilitiesBPR1);
	$idrole2 = $_POST['idrole2BP'];
	$capabilitiesBPR2 = $_POST['capabilitiesBPR2'];
	$listCapabilitiesR2 = explode(" ", $capabilitiesBPR2);
	
	if (empty($name) || empty($bptId)){
		$errorput6="Name or Bpt_id is invalid";
	}else{

		//get token from keycloak
		$token=get_token();

		//call to REST
		//using a standard class to save data
		$listRoles = array();
		
		if (!empty($idrole1) && !empty($capabilitiesBPR1)){
			$role = new stdClass;
			$role->id = $idrole1;
			$role->listCapabilities=$listCapabilitiesR1;
			array_push($listRoles, $role);
		}
		
		if (!empty($idrole2) && !empty($capabilitiesBPR2)){
			$role = new stdClass;
			$role->id = $idrole2;
			$role->listCapabilities=$listCapabilitiesR2;
			array_push($listRoles, $role);
		}
				
		$businessProcess = new stdClass;
		$businessProcess->businessProcess ->name = $name;
		$businessProcess->businessProcess ->bptId = $bptId;
		$businessProcess->businessProcess-> listRoles = $listRoles;
		$errorput6=updateBusinessProcessRole($token, $businessProcess);
	}

}

if (isset($_POST['submitBusiness11'])) {
	
	$value = $_POST['idCapabilityTypeD'];
	
	if (empty($value)){
		$errordel = "Id Capability Type is empty";
	}else{
		//get token from keycloak
		$token=get_token();

		$errordel=RemoveCapabilityType($token, $value);
	}
	
}

if (isset($_POST['submitBusiness12'])) {

	$value = $_POST['idCapabilityD'];
	
	if (empty($value)){
		$errordel = "Id Capability is empty";
	}else{
		//get token from keycloak
		$token=get_token();

		$errordel=RemoveCapability($token, $value);
	}
	
}

if (isset($_POST['submitBusiness13'])) {

	$value = $_POST['idCBusinessD'];
	
	if (empty($value)){
		$errordel = "Id Business Process is empty";
	}else{
		//get token from keycloak
		$token=get_token();

		$errordel=RemoveBusinessProcess($token, $value);
	}
	
}

if (isset($_POST['submitBusiness14'])) {

	$value = $_POST['idCBusinessTemplateD'];
	
	if (empty($value)){
		$errordel = "Id Business Process Template is empty";
	}else{
		//get token from keycloak
		$token=get_token();

		$errordel=RemoveBusinessProcessTemplate($token, $value);
	}
	
}

if (isset($_POST['submitBusiness16'])) {
	$body = '' ;

	//get token from keycloak
	$token=get_token();

	//call to REST
	//using a standard class to save data
	$receiveShipmentStatusRequestMessage = new stdClass;
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->businessProcessId = "1";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->status = "Announced";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->reason = "1";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->messageId = "1";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->senderId = "1";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->receiverId = "1";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->senderAppType = "1";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->receiverAppType = "1";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->shipmentId = "1";
	$receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->shipmentDataLink = "1";
	$erroruse=postRequestMessage($token, "receive_shipment_status", $receiveShipmentStatusRequestMessage);
}

function get_token(){
	$token="";
		
	$token=$_POST['token'];
	
	
	return $token;
}

?>