//
// Version: 0.16.0
//
var loadData = function () {
	//alert('keycloak function' + keycloak.token);
	document.getElementById('token').value = keycloak.token;
    
};

var loadFailure = function () {
    document.getElementById('status').innerHTML = '<b>Failed to load data.  Check console log</b>';
};

var reloadData = function () {
    console.log('ReloadData');
    keycloak.updateToken(10)
    .success(loadData)
    .error(function() {
        document.getElementById('status').innerHTML = '<b>Error updating Token. Failed to load data.  User is logged out.</b>';
    });
}
//-------------------------------------------------------------------------------------------------------------

