<?php
//
//version: 0.16.0
//

include('testcontroller.php'); // Includes Login Script
?>
<!DOCTYPE html>
<html>
	<head>
	<title>Test SDI Capabilities</title>
	<script src="http://auth.pie.fispace.eu:8080/auth/js/keycloak.js"></script>
	<script>var keycloak = Keycloak('keycloak.json');</script>
	<script src="js/functions.js"></script>
	
	<!-- <link href="css/styles.css" rel="stylesheet" type="text/css">-->
	</head>
	<body>
	<div id="main">
	<center><h2>PHP Connector to SDI Resources</h2></center>
	<form id="form1" action="" method="post">
	Token: <input type="text" name="token" id="token" value="<?php echo $token;?>" placeholder="Press Login until obtain Token" readonly="true" size="100"> 
	<div id="login">
	<h2>Keycloak's Credentials</h2>
	<table>
	<tr>
		<td width="50%">
			<table>
				<tr>
					<td><input name="submit" type="button" value=" Login " onclick="submitLogin() "></td>
					<td><input name="logout" type="button" value=" Logout " onclick="submitLogout() "></td>
				</tr>
			</table>
		</td>
		<td width="50%">
		</td>
	</tr>
	</table>
	<span><?php echo $error; ?>
	<span id="status""></span>
	</div>
		
	<h2>GET Methods</h2>
	<br>
	<div id="login">
	<table>
		<tr>
			<td width="40%">
				<table>
					<tr>
						<td></td>
						<td><input name="submitBusiness0" type="submit" value=" Get Capability Types "  style="width:250px"></td>
					</tr>
					<tr>
						<td>
							<input id="idCapabilityType" name="idCapabilityType" placeholder="Id Capability Type" type="text" size="20">
						</td>
						<td>
						<input name="submitBusiness2" type="submit" value=" Get Capability Type " style="width:250px">
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input name="submitBusiness4" type="submit" value=" Get Capabilities "  style="width:250px"></td>
					</tr>
					<tr>
						<td>
							<input id="idCapability" name="idCapability" placeholder="Id Capability " type="text" size="20">
						</td>
						<td>
						<input name="submitBusiness5" type="submit" value=" Get Capability " style="width:250px">
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input name="submitBusiness1" type="submit" value=" Get Business Processes " style="width:250px"></td>
					</tr>
					<tr>
						<td>
							<input id="idCBusiness" name="idBusiness" placeholder="Id Business Process" type="text" size="20">
						</td>
						<td>
						<input name="submitBusiness3" type="submit" value=" Get Business Process " style="width:250px">
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input name="submitBusiness6" type="submit" value=" Get Business Process Templates " style="width:250px"></td>
					</tr>
					<tr>
						<td>
							<input id="idCBusinessTemplate" name="idBusinessTemplate" placeholder="Id Business Process Template" type="text" size="20">
						</td>
						<td>
						<input name="submitBusiness15" type="submit" value=" Get Business Process Template" style="width:250px">
						</td>
					</tr>
				</table>
			</td>
			<td width="60%">
				<textarea name="txtCapability" cols="70" rows="15"><?php echo $text0; ?></textarea>
			</td>
		</tr>
	</table>
	<span><?php echo $errorget; ?>
	</div>	
	
	<div id="login">
	<h2>PUT</h2>
	<h2>New Capability</h2>
	<br>
	<input id="descCapability" name="descCapability" placeholder="Description " type="text" size="20">
	<input id="uriCapability" name="uriCapability" placeholder="URI " type="text" size="20">
	<input id="cteCapability" name="cteCapability" placeholder="Capability Type " type="text" size="20">
	<input name="submitBusiness7" type="submit" value=" Submit " style="width:100px">
	<br>
	<span><?php echo $errorput; ?></span>
	</div>
	
	<div id="login">
	<h2>New Capability Type</h2>
	<br>
	<input id="nameCapabilityType" name="nameCapabilityType" placeholder="Name " type="text" size="20">
	<input id="reqmsgtypeCapabilityType" name="reqmsgtypeCapabilityType" placeholder="Request Message Type " type="text" size="20">
	<input id="resmsgtypeCapabilityType" name="resmsgtypeCapabilityType" placeholder="Response Message Type " type="text" size="20">
	<input id="schemaCapabilityType" name="schemaCapabilityType" placeholder="Schema Location " type="text" size="20">
	<input id="contextCapabilityType" name="contextCapabilityType" placeholder="Context Path " type="text" size="20">
	<input name="submitBusiness8" type="submit" value=" Submit " style="width:100px">
	<br>
	<span><?php echo $errorput2; ?></span>
	</div>
	
	<div id="login">
	<h2>New Business Process Template</h2>
	<br>
	<input id="nameBPT" name="nameBPT" placeholder="Name " type="text" size="20">
	<label for="capabilitiesBPT">Capability Types IDs:</label>
	<input id="capabilitiesBPT" name="capabilitiesBPT" placeholder="Separate each id with a space " type="text" size="20">
	<input name="submitBusiness9" type="submit" value=" Submit " style="width:100px">
	<br>
	<span><?php echo $errorput3; ?></span>
	</div>
	
	<div id="login">
	<h2>New Business Process Template with Roles</h2>
	<br>
	<label for="nameBPTR">Name:</label>
	<input id="nameBPTR" name="nameBPTR" placeholder="Name " type="text" size="20">
	<br>
	<label for="idrole1BPT">Role 1 ID:</label>
	<input id="idrole1BPT" name="idrole1BPT" placeholder="Id Role 1 " type="text" size="20">
	<label for="capabilitiesBPTR1">Capability Types IDs Role 1:</label>
	<input id="capabilitiesBPTR1" name="capabilitiesBPTR1" placeholder="Separate each id with a space " type="text" size="20">
	<br>
	<label for="idrole2BPT">Role 2 ID:</label>
	<input id="idrole2BPT" name="idrole2BPT" placeholder="Id Role 2 " type="text" size="20">
	<label for="capabilitiesBPTR2">Capability Types IDs Role 2:</label>
	<input id="capabilitiesBPTR2" name="capabilitiesBPTR2" placeholder="Separate each id with a space " type="text" size="20">
	<input name="submitBusiness17" type="submit" value=" Submit " style="width:100px">
	<br>
	<span><?php echo $errorput5; ?></span>
	</div>
	
	<div id="login">
	<h2>New Business Process</h2>
	<br>
	<input id="nameBP" name="nameBP" placeholder="Name " type="text" size="20">
	<input id="bptId" name="bptId" placeholder="Bpt_id " type="text" size="20">
	<label for="capabilitiesBP">Capability IDs:</label>
	<input id="capabilitiesBP" name="capabilitiesBP" placeholder="Separate each id with a space " type="text" size="20">
	<input name="submitBusiness10" type="submit" value=" Submit " style="width:100px">
	<br>
	<span><?php echo $errorput4; ?></span>
	</div>
	
	<div id="login">
	<h2>New Business Process with Roles</h2>
	<br>
	<input id="nameBPR" name="nameBPR" placeholder="Name " type="text" size="20">
	<input id="bptIdR" name="bptIdR" placeholder="Bpt_id " type="text" size="20">
	<br>
	<label for="idrole1BP">Role 1 ID:</label>
	<input id="idrole1BP" name="idrole1BP" placeholder="Id Role 1 " type="text" size="20">
	<label for="capabilitiesBPR1">Capability IDs Role1:</label>
	<input id="capabilitiesBPR1" name="capabilitiesBPR1" placeholder="Separate each id with a space " type="text" size="20">
	<br>
	<label for="idrole2BP">Role 2 ID:</label>
	<input id="idrole2BP" name="idrole2BP" placeholder="Id Role 2 " type="text" size="20">
	<label for="capabilitiesBPR2">Capability IDs Role2:</label>
	<input id="capabilitiesBPR2" name="capabilitiesBPR2" placeholder="Separate each id with a space " type="text" size="20">
	<input name="submitBusiness18" type="submit" value=" Submit " style="width:100px">
	<br>
	<span><?php echo $errorput6; ?></span>
	</div>
	
	<h2>DELETE</h2>
	<br>
	<div id="login">
	<table>
		<tr>
			<td width="40%">
				<table>
					<tr>
						<td>
							<input id="idCapabilityTypeD" name="idCapabilityTypeD" placeholder="Id Capability Type" type="text" size="20">
						</td>
						<td>
						<input name="submitBusiness11" type="submit" value=" Delete Capability Type " style="width:250px">
						</td>
					</tr>
					<tr>
						<td>
							<input id="idCapabilityD" name="idCapabilityD" placeholder="Id Capability " type="text" size="20">
						</td>
						<td>
						<input name="submitBusiness12" type="submit" value=" Delete Capability " style="width:250px">
						</td>
					</tr>
					<tr>
						<td>
							<input id="idCBusinessD" name="idCBusinessD" placeholder="Id Business Process" type="text" size="20">
						</td>
						<td>
						<input name="submitBusiness13" type="submit" value=" Delete Business Process " style="width:250px">
						</td>
					</tr>
					<tr>
						<td>
							<input id="idCBusinessTemplateD" name="idCBusinessTemplateD" placeholder="Id Business Process Template" type="text" size="20">
						</td>
						<td>
						<input name="submitBusiness14" type="submit" value=" Delete Business Process Template" style="width:250px">
						</td>
					</tr>
				</table>
			</td>
			<td width="60%">
				
			</td>
		</tr>
	</table>
	<span><?php echo $errordel; ?>
	</div>
	
	<h2>USE</h2>
	<br>
	<div id="login">
	<table>
		<tr>
			<td width="40%">
				<table>
					<tr>
						<td>
							
						</td>
						<td>
						<input name="submitBusiness16" type="submit" value=" USE Method " style="width:250px">
						</td>
					</tr>
				</table>
			</td>
			<td width="60%">
				
			</td>
		</tr>
	</table>
	<span><?php echo $erroruse; ?>
	</div>
	
		
	</form>
	
	</div>
	
	
	 
    <script>

    function submitLogin(){
    	keycloak.init({ onLoad: 'login-required' }).success(reloadData).error(function() {
        	document.getElementById('status').innerHTML = '<b>Error in login at keycloak.init</b>';
    	});	
    	
		document.form.submit();
            
    }

    function submitLogout(){
		alert('fora');        
     	keycloak.logout();
    }
      
    </script>
    </body> 
</html>