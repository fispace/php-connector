<?php
//
//version: 0.16.0
//

include('httpful.phar');
include_once("constants.php");

////////////////////////////////////////////////////////////////////////////////////
//GET Methods
////////////////////////////////////////////////////////////////////////////////////
function getCapabilityTypes($token){
	$response='';
	$resourceuri = "capability-types";
	$finaluri = BASE_URI . $resourceuri;
	
	$response=getMethod($finaluri, "application/xml", $token);
		
	return $response; 
}

function getCapabilityType($token, $value){
	$response='';
	
	if (isset($value)){
		$resourceuri = "capability-types";
		$finaluri = BASE_URI . $resourceuri . "/" . $value;
	
		$response=getMethod($finaluri, "application/xml", $token);
	}

	return $response;
}

function getCapabilities($token){
	$response='';
	$resourceuri = "capabilities";
	$finaluri = BASE_URI . $resourceuri;

	$response=getMethod($finaluri, "application/xml", $token);

	return $response;
}

function getCapability($token, $value){
	$response='';

	if (isset($value)){
		$resourceuri = "capabilities";
		$finaluri = BASE_URI . $resourceuri . "/" . $value;

		$response=getMethod($finaluri, "application/xml", $token);
	}

	return $response;
}

function getBusinessProcesses($token){
	$response='';
	$resourceuri = "business-processes";
	$finaluri = BASE_URI . $resourceuri;

	$response=getMethod($finaluri, "application/xml", $token);

	return $response;
}

function getBusinessProcess($token, $value){
	$response='';

	if (isset($value)){
		$resourceuri = "business-processes";
		$finaluri = BASE_URI . $resourceuri . "/" . $value;

		$response=getMethod($finaluri, "application/xml", $token);
	}

	return $response;
}

function getBusinessProcessTemplates($token){
	$response='';
	$resourceuri = "business-process-templates";
	$finaluri = BASE_URI . $resourceuri;

	$response=getMethod($finaluri, "application/xml", $token);

	return $response;
}

function getBusinessProcessTemplate($token, $value){
	$response='';

	if (isset($value)){
		$resourceuri = "business-process-templates";
		$finaluri = BASE_URI . $resourceuri . "/" . $value;

		$response=getMethod($finaluri, "application/xml", $token);
	}

	return $response;
}

//Generic GET Method
function getMethod($url, $content, $token){
	
	try{
		$response = \Httpful\Request::get($url)
		->addHeader('Content-Type', $content)
		->addHeader('Accept', $content)
		->addHeader('Authorization', 'Bearer ' . $token)
		->send();
	}catch(Exception $e){
		$response="Error to access to " . $url;
	}
	
	return $response;
}

////////////////////////////////////////////////////////////////////////////////////
//PUT Methods
////////////////////////////////////////////////////////////////////////////////////
function updateCapability($token, $capability){
	$response='';
	$resourceuri = "capabilities";
	$finaluri = BASE_URI . $resourceuri;
		
	$body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . 
		'<Capability xmlns="http://www.limetri.eu/schemas/ygg">' .
		'<description>' . $capability->Capability->description . '</description>' .
		'<uri>' . $capability->Capability->uri . '</uri>' .
		'<cte_id>' . $capability->Capability->cte_id . '</cte_id>' .
		'</Capability>'; 
	
	$response=putMethod($finaluri, $body, $token, "application/xml");
		
	return $response;  
	
}

function updateCapabilityType($token, $capabilityType){
	$response='';
	$resourceuri = "capability-types";
	$finaluri = BASE_URI . $resourceuri;

	$body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
			'<capabilityType xmlns="http://www.limetri.eu/schemas/ygg">' .
			'<name>' . $capabilityType->capabilityType->name . '</name>' .
			'<requestMessageType>' . $capabilityType->capabilityType->requestMessageType . '</requestMessageType>' .
			'<responseMessageType>' . $capabilityType->capabilityType->responseMessageType . '</responseMessageType>' .
			'<schemaLocation>' . $capabilityType->capabilityType->schemaLocation . '</schemaLocation>' .
			'<contextPath>' . $capabilityType->capabilityType->contextPath . '</contextPath>' .
			'</capabilityType>';
	
	$response=putMethod($finaluri, $body, $token, "application/xml");

	return $response;

}

function updateBusinessProcessTemplate($token, $businessProcessTemplate){
	$response='';
	$resourceuri = "business-process-templates";
	$finaluri = BASE_URI . $resourceuri;
			
	$body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
			'<BusinessProcessTemplate xmlns="http://www.limetri.eu/schemas/ygg">' .
			'<name>' . $businessProcessTemplate->businessProcessTemplate->name . '</name>';
			
	$list = $businessProcessTemplate->businessProcessTemplate-> listCapabilityTypes;
	foreach ($list as $value) {
		$body = $body . '<capabilityTypes>';
		$body = $body . '<id>' . $value . '</id>' .
				'<name>name</name>' .
				'<requestMessageType>request</requestMessageType>' .
				'<responseMessageType>response</responseMessageType>' .
				'<schemaLocation>schema</schemaLocation>' .
				'<contextPath>eu.fispace.api.lg</contextPath>';
		$body = $body . '</capabilityTypes>';
	}
		
	$body = $body . '</BusinessProcessTemplate>';
	
	$response=putMethod($finaluri, $body, $token, "application/xml");

	return $response;

}

function updateBusinessProcessTemplateRole($token, $businessProcessTemplate){
	$response='';
	$resourceuri = "business-process-templates";
	$finaluri = BASE_URI . $resourceuri;
		
	$body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
			'<BusinessProcessTemplate xmlns="http://www.limetri.eu/schemas/ygg">' .
			'<name>' . $businessProcessTemplate->businessProcessTemplate->name . '</name>';
		
	
	$listRoles = $businessProcessTemplate->businessProcessTemplate-> listRoles;
	foreach ($listRoles as $value) {
		if (!empty($value)){
			$body = $body . '<role><id>'. $value->id . '</id>';
			$listCapabilityTypes = $value->listCapabilityTypes;
			foreach ($listCapabilityTypes as $valuecap) {
				if (!empty($valuecap)){
					$body = $body . '<capabilityType>' .
		                '<id>' . $valuecap . '</id>' .
		                '<name>name</name>' .
		                '<requestMessageType>request</requestMessageType>' .
		                '<responseMessageType>response</responseMessageType>' .
		                '<schemaLocation>schema</schemaLocation>' .
		                '<contextPath>eu.fispace.api.lg</contextPath>' .
		                '</capabilityType>';
				}
			}
			$body = $body . '</role>';
		}
	}
	
	$body = $body . '</BusinessProcessTemplate>';

	$response=putMethod($finaluri, $body, $token, "application/xml");
	 
	return $response;

}

function updateBusinessProcess($token, $businessProcess){
	$response='';
	$resourceuri = "business-processes";
	$finaluri = BASE_URI . $resourceuri;
	$capability='';
	
	$body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
			'<BusinessProcess xmlns="http://www.limetri.eu/schemas/ygg">' .
			'<name>' . $businessProcess->businessProcess->name . '</name>' .
			'<bpt_id>' . $businessProcess->businessProcess->bptId . '</bpt_id>';
			
	$list = $businessProcess->businessProcess-> listCapabilities;
	foreach ($list as $value) {
				
		//Find capability
		$capability = getCapability($token, $value);
		$xml = simplexml_load_string($capability);
		$result = $xml->xpath("ns2:cte_id");
		while(list( , $node) = each($result)) {
			$cte_id = $node;
			
			$body = $body . '<capabilities>'; 
			$body = $body . '<id>' . $value . '</id>' .
				'<description>bpdesc</description>' .
				'<uri>urn:uri</uri>' .
				'<cte_id>' . $cte_id . '</cte_id>';
			$body = $body . '</capabilities>';
		}
	
	
	}
	
	$body = $body . '</BusinessProcess>';

	$response=putMethod($finaluri, $body, $token, "application/xml");

	return $response;

}

function updateBusinessProcessRole($token, $businessProcess){
	$response='';
	$resourceuri = "business-processes";
	$finaluri = BASE_URI . $resourceuri;
	$capability='';

	$body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
			'<BusinessProcess xmlns="http://www.limetri.eu/schemas/ygg">' .
			'<name>' . $businessProcess->businessProcess->name . '</name>' .
			'<bpt_id>' . $businessProcess->businessProcess->bptId . '</bpt_id>';
		
	$listRoles = $businessProcess->businessProcess-> listRoles;
	foreach ($listRoles as $value) {
		if (!empty($value)){
			$body = $body . '<role><id>'. $value->id . '</id>';
			$listCapabilities = $value->listCapabilities;
			foreach ($listCapabilities as $valuecap) {
				if (!empty($valuecap)){
					//Find capability
					$capability = getCapability($token, $valuecap);
					$xml = simplexml_load_string($capability);
					$result = $xml->xpath("ns2:cte_id");
					while(list( , $node) = each($result)) {
						$cte_id = $node;
													
						$body = $body . '<capability>';
						$body = $body . '<id>' . $valuecap . '</id>' .
								'<description>bpdesc</description>' .
								'<uri>urn:uri</uri>' .
								'<cte_id>' . $cte_id . '</cte_id>';
						$body = $body . '</capability>';
					}
				}
			}
			$body = $body . '</role>';
		}
	}
	
	$body = $body . '</BusinessProcess>';

	$response=putMethod($finaluri, $body, $token, "application/xml");

	return $response;

}

//Generic PUT method
function putMethod($url, $body, $token, $content){
	$response='';
	
	try{
		$response = \Httpful\Request::put($url)               
		->addHeader('Content-Type', $content)
		->addHeader('Accept', $content)
		->addHeader('Authorization', 'Bearer ' . $token)  
		->body($body)             
		->send();
	}catch(Exception $e){
		$response=$e->getMessage();
	}

	return $response;
}

////////////////////////////////////////////////////////////////////////////////////
//DELETE Methods
////////////////////////////////////////////////////////////////////////////////////

function removeCapabilityType($token, $value){
	$response='';

	if (isset($value)){
		$resourceuri = "capability-types";
		$finaluri = BASE_URI . $resourceuri . "/" . $value;

		$response=delMethod($finaluri, "application/xml", $token);
	}

	return $response;
}

function removeCapability($token, $value){
	$response='';

	if (isset($value)){
		$resourceuri = "capabilities";
		$finaluri = BASE_URI . $resourceuri . "/" . $value;

		$response=delMethod($finaluri, "application/xml", $token);
	}

	return $response;
}

function removeBusinessProcess($token, $value){
	$response='';

	if (isset($value)){
		$resourceuri = "business-processes";
		$finaluri = BASE_URI . $resourceuri . "/" . $value;

		$response=delMethod($finaluri, "application/xml", $token);
	}

	return $response;
}

function removeBusinessProcessTemplate($token, $value){
	$response='';

	if (isset($value)){
		$resourceuri = "business-process-templates";
		$finaluri = BASE_URI . $resourceuri . "/" . $value;

		$response=delMethod($finaluri, "application/xml", $token);
	}

	return $response;
}

//Generic DELETE method
function delMethod($url, $content, $token){
	$response='';

	try{
		$response = \Httpful\Request::delete($url, $content)
		->addHeader('Authorization', 'Bearer ' . $token)
		->send();
	}catch(Exception $e){
		$response=$e->getMessage();
	}

	return $response;
}

////////////////////////////////////////////////////////////////////////////////////
//POST Methods
////////////////////////////////////////////////////////////////////////////////////
function postRequestMessage($token, $name, $receiveShipmentStatusRequestMessage){
	$response='';
	$resourceuri = "capabilities/" . $name . "/use";
	$finaluri = BASE_URI_USE . $resourceuri;
	
	$body='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' .
	'<ns3:ReceiveShipmentStatusRequestMessage xmlns:ns2="http://www.limetri.eu/schemas/ygg" xmlns:ns3="http://www.fispace.eu/domain/lg">' .
	'<ns2:businessProcessId>' . $receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->businessProcessId . '</ns2:businessProcessId>' . 
	'<messageId>' . $receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->messageId . '</messageId>' .
	'<senderId>' . $receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->senderId . '</senderId>' .
	'<receiverId>' . $receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->receiverId . '</receiverId>' .
	'<senderAppType xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>' .
	'<receiverAppType xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>' .
	'<shipmentId>' . $receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->shipmentId . '</shipmentId>' .
	'<shipmentDataLink>' . $receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->shipmentDataLink . '</shipmentDataLink>' .
	'<status>' . $receiveShipmentStatusRequestMessage->receiveShipmentStatusRequestMessage->status . '</status>' .
	'</ns3:ReceiveShipmentStatusRequestMessage>';
	
	$response=postMethod($finaluri, $body, $token, "application/xml");

	return $response;

}


//Generic POST method
function postMethod($url, $body, $token, $content){
	$response='';

	try{
		$response = \Httpful\Request::post($url)
		->addHeader('Content-Type', $content)
		->addHeader('Accept', $content)
		->addHeader('Authorization', 'Bearer ' . $token)
		->body($body)
		->send();
	}catch(Exception $e){
		$response=$e->getMessage();
	}

	return $response;
}

?>